import subprocess
import socket

HOST = ''
PORT = 54000
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
devices_count = 2
while True:
    conn, addr = s.accept()
    container_id = conn.recv(1024)
    if container_id != '':
        print(container_id)
        devices_count = devices_count - 1

    if devices_count <= 0:
        print("Container ID received: %s", container_id)
        cmd0 = subprocess.Popen("docker stop %s"% container_id, \
                                stdout=subprocess.PIPE, shell=True)
        (output, err0) = cmd0.communicate()
        cmd1 = subprocess.Popen("docker commit %s %s:latest"% (container_id, \
                                    container_id), stdout=subprocess.PIPE, \
                                    shell=True)
        (output, err1) = cmd1.communicate()
        image_id = output.strip("\n")[:14]
        cmd2 = subprocess.Popen("docker save %s > %s.rar"%(image_id, image_id), \
                                    stdout=subprocess.PIPE, shell=True)
        (output, err2) = cmd2.communicate()
        cmd3 = subprocess.Popen("docker rm %s"% container_id,
                                    stdout=subprocess.PIPE, shell=True)
        (output, err3) = cmd3.communicate()
        if err0 or err1 or err2 or err3:
            break
        print("RAR file created")
        
        container_id = ''
        devices_count = 2

    conn.send(container_id)
    conn.close()
