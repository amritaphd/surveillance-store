#!/usr/bin/python3
"""
Configuration file.
"""
import random
import multiprocessing

from helper import Container
from streams import AcceptStream, CreateLog
from time import sleep

stream_details = {'1':'rtsp://root:amritakripa@10.30.9.46/axis-media/media.amp',
                  '2':'rtsp://admin:amma123@10.30.9.202:554/'}
use_tcp = False


log_size = 104857
tenant_id = 1
tenant_url_update = 'http://10.30.11.177:8888/gettenantdetails'
policy_url_details = 'http://10.30.11.177:8888/getpolicydetails'
log_url_add = 'http://10.30.11.177:8888/addlog'

process_list = []

def stream(device_id, stream_url):
    container_obj = Container()
    container_id = container_obj.get_container_id()
    initial_logfilename = device_id+"_"+str(random.getrandbits(256))
    fOut = open(initial_logfilename, 'wb')
    fOut.close()
    cam_stream = AcceptStream(stream_url, use_tcp)
    cam_stream.start()
    cam_log = CreateLog(initial_logfilename, None, log_size, tenant_id, container_id, \
                54000, tenant_url_update, policy_url_details, log_url_add, \
                device_id)
    cam_log.start()
    sleep(1)


if __name__ == "__main__":

    for device_id, stream_url in stream_details.items():
        stream(device_id, stream_url)
    #    process = multiprocessing.Process(target=stream, args=[device_id, stream_url])
    #    process.start()
        #process_list.append(process)
