from twisted.internet import reactor, protocol

class EchoClient(protocol.Protocol):
    def connectionMade(self):
        self.transport.write("Hello World")

    def dataReceived(self, data):
        print("Server Said %s" % data)

class EchoFactory(protocol.ClientFactory):
    def buildProtocol(self, addr):
        return EchoClient()

    def clientConnectionFailed(self, connector, reason):
        print("Connection Failed")
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("Connection Lost")
        reactor.stop()

reactor.connectTCP("172.17.42.1", "9000", EchoFactory())
reactor.run()
