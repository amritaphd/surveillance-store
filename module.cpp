import sys
import live555
import random
import os
import pickle

from threading import Thread, Lock
from time import strftime, localtime, sleep
from .helper import PolicyInfo, HostInfo, Trigger, Log

stream_buffer = []
current_filesize = 0
lock = Lock()
t = Thread()

class AcceptStream(Thread):
    """
    Accept RTSP streams from the security cameras and push into a stream buffer.
    The stream buffer will be read by another module and written it into a file.
    The file will be divided into different chunks and transported across the
    network finally into a storage medium.
    """

    def __init__(self, stream_url, use_tcp):
        super(AcceptStream, self).__init__()
        self.video_stream_url = stream_url
        self.use_tcp = use_tcp

    def run(self):
        self.start_stream()

    def start_stream(self):
        global t
        live555.startRTSP(self.video_stream_url, self.read_video_frame, \
                            self.use_tcp)
        t = Thread(target=live555.runEventLoop, args=())
        t.setDaemon(True)
        t.start()
        t.join()

    def read_video_frame(self, codecName, bytes, sec, usec, durUSec):
        lock.acquire()
        global stream_buffer
        global current_filesize
        global total_size
        stream_buffer.append(bytes)
        current_filesize = current_filesize + len(bytes)
        total_size = total_size + len(bytes)
        lock.release()
        sleep(random.random())

class CreateLog(Thread):
    """
    The received streams are written into a log file. The log file will be divi-
    ded into different chunk. The chunks are named with a 256 bits string and
    the details are updated to the metadata server.
    """

    def __init__(self, log_filename, log_prev, filesize, tenant_id, \
                    container_id, port):
        super(CreateLog, self).__init__()
        self.log_filename = log_filename
        self.log_prev = log_prev
        self.filesize = 1048576
        self.tenant_url = 'http://10.30.11.173:8888/gettenantdetails'
        self.policy_url = 'http://10.30.11.173:8888/getpolicydetails'
        self.log_url = 'http://10.30.11.173:8888/addlog'
        self.tenant_id = tenant_id
        self.container_id = container_id
        self.port = port

    def run(self):
        self.read_stream_buffer()

    def read_stream_buffer(self):
        policy = PolicyInfo(self.tenant_id, self.log_filename, \
                                self.tenant_urli, self.policy_url)
        host = HostInfo()
        trigger_migration = Trigger(port, host.get_host_details(), \
                                        self.container_id)
        log_obj = Log(self.log_url)
        while True:
            lock.acquire()
            global current_filesize
            global total_size
            global t

            if total_size > 0 and (total_size / 1024) > 1:
                status = log_obj.add_log(self.log_filename, self.log_prev)
                if not status:
                    print("Unable to update the log details")
                    break
                t.stop()
                trigger_migration.send_signal()

            if stream_buffer:
                if current_filesize > self.filesize:
                    status = log_obj.add_log(self.log_filename, self.log_prev)
                    if not status:
                        print("Unable to update the log")
                        break
                    self.log_prev = self.log_filename
                    self.log_filename = head_filename+'_'+ \
                                            str(random.getrandbits(256))
                    fOut = open(self.log_filename, 'ab')
                    policy_details = policy.get_policy_details()
                    header = pickle.dumps(policy_details)
                    fOut.write(header)
                    current_filesize = 0
                else:
                    fOut = open(self.log_filename, 'ab')

                for i in stream_buffer:
                    fOut.write(b'\0\0\0\1' + i)
                    stream_buffer.pop()
                fOut.close()

            lock.release()
            sleep(random.random())


head_filename = "headfile"
fOut = open(head_filename, 'wb')
fOut.close()
container_obj = Container()
container_id = container_obj.get_container_id()
AcceptStream('rtsp://root:amritakripa@10.30.9.46/axis-media/media.amp', \
                False).start()
CreateLog(head_filename, None, 1, 1, container_id, 54000).start()
