import os
import optparse

from twisted.internet import reactor, protocol, stdio, defer
from twisted.protocols import basic
from twisted.internet.protocol import ClientFactory, Protocol

from helper import get_file_md5_hash, read_bytes_from_file

class ContainerMigrateClient(basic.LineReceiver):

    def __init__(self, server_ip, server_port, file_path, file_name):
        self.server_ip = server_ip
        self.server_port = server_port
        self.file_path = file_path
        self.file_name = file_name

    def connectionMade(self):
        if not os.path.isfile(self.file_path):
            self._display_message('This file does not exist')
            return

        file_size = os.path.getsize(self.file_path) / 1024
        file_hash = get_file_md5_hash(self.file_path)

        print 'Uploading file: %s (%d KB)' % (self.file_name, file_size)

        connection.transport.write(self.file_name+'\r\n')

        connection.transport.write(file_hash+'\r\n')

        self.setRawMode()

        connection.transport.write('\r\n')
        for bytes in read_bytes_from_file(self.file_path):
            connection.transport.write(bytes)

        connection.transport.write(file_hash)

            # When the transfer is finished, we go back to the line mode
        self.setLineMode()

class ContainerMigrateFactory(ClientFactory):
    def buildProtocol(self, addr):
        return ContainerMigrateClient('10.30.11.174', 54002, \
                        '/home/snowman/PhD/surveillance/d320444384a1a7.rar', \
                        'd320444384a1a7.rar')

    def clientConnectionFailed(self, connector, reason):
        print "Connection failed."
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print "Connection lost."
        reactor.stop()

connection = reactor.connectTCP("10.30.11.174", 54002, ContainerMigrateFactory())
reactor.run()
