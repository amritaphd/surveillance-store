import requests
import socket
import subprocess

from time import strftime, localtime

class PolicyInfo():
    """
    Helper class which can retrieve policy details from metadata server.
    """

    def __init__(self, tenant_id, tenant_url, policy_url, log_filename, \
                    device_id):
        self.tenant_id = tenant_id
        self.log_filename = log_filename
        self.tenant_url  = tenant_url
        self.policy_url = policy_url
        self.device_id = device_id

    def get_policy_details(self):
        tenant_payload = {'tenant_id': self.tenant_id}
        tenant_response = requests.post(self.tenant_url, data = tenant_payload)
        tenant_details = tenant_response.json()
        policy_id = tenant_details['policy_id']
        policy_payload = {'policy_id': policy_id}
        policy_response = requests.post(self.policy_url, data = policy_payload)
        policy_details = policy_response.json()
        policy_details['creation_date'] = strftime("%a, %d %b %Y %H:%M:%S",\
                                                        localtime())
        policy_details['parent_id'] = self.log_filename
        policy_details['device_id'] = self.device_id
        return policy_details


class HostInfo():
    """
    Class to retrieve host IP address from inside a container.
    """
    def __init__(self, host_ip = None):
        self.host_ip = host_ip

    def get_host_details(self):
        ps = subprocess.Popen(('/sbin/ip', 'route'), stdout=subprocess.PIPE)
        output = subprocess.check_output(('awk', '/default/ { print $3}'), \
                                                stdin=ps.stdout)
        host = str(output)[2:-1].rstrip("\\n")
        return host


class Trigger():
    """
    To send signals to the deamon running on the host machines.
    """
    def __init__(self, port, host, signal):
        self.port = port
        self.host = host
        self.signal = signal

    def send_signal(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host, self.port))
        s.sendall(bytes(self.signal, 'UTF-8'))
        data = s.recv(1024)
        s.close()


class Container():
    """
    Class to provide the container information like container id.
    """

    def __init__(self, container_id = None):
        self.container_id = container_id

    def get_container_id(self):
        cmd1 = subprocess.Popen(['cat', '/proc/self/cgroup'], \
                                    stdout=subprocess.PIPE)
        cmd2 = subprocess.Popen(['grep', 'cpu:/'], stdin=cmd1.stdout, \
                                    stdout=subprocess.PIPE)
        cmd1.stdout.close()
        cmd3 = subprocess.Popen(['sed', 's/\([0-9]\):cpu:\/docker\///g'], \
                                    stdin=cmd2.stdout,stdout=subprocess.PIPE)
        cmd2.stdout.close()
        output = cmd3.communicate()[0]
        self.container_id = str(output)[2:14]
        return self.container_id


class Log():
    """
    Provide the Log information like location and metadata included in the log.
    """
    def __init__(self, container_id, log_url):
        self.log_url = log_url
        self.container_id = container_id

    def add_log(self, log_filename, log_prev):
        payload = {'log_id':log_filename, 'parent_id':log_prev, 'container_id':self.container_id}
        log_response = requests.post(self.log_url, data = payload)
        log_details = log_response.json()

        if log_details['valid'] == True:
	           return True

        return False
